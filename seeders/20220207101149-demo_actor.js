"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "movies",
      [
        {
          id: 1,
          name: "PK",
          updatedAt: "2022-02-07T09:30:42.599Z",
          createdAt: "2022-02-07T09:30:42.599Z",
        },
        {
          id: 2,
          name: "Bodyguard",
          updatedAt: "2022-02-07T09:30:42.599Z",
          createdAt: "2022-02-07T09:30:42.599Z",
        },
        {
          id: 3,
          name: "Suryavansi",
          updatedAt: "2022-02-07T09:30:42.599Z",
          createdAt: "2022-02-07T09:30:42.599Z",
        },
        {
          id: 4,
          name: "Bang Bang",
          updatedAt: "2022-02-07T09:30:42.599Z",
          createdAt: "2022-02-07T09:30:42.599Z",
        },
        {
          id: 5,
          name: "KGF",
          updatedAt: "2022-02-07T09:30:42.599Z",
          createdAt: "2022-02-07T09:30:42.599Z",
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("movies", null, {});
  },
};
