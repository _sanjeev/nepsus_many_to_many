const { sequelize, Actor, Movie, MovieActor } = require("./models");
const express = require("express");

const app = express();
app.use(express.json());

app.post("/movie", async (req, res) => {
  const { name } = req.body;
  try {
    const movie = await Movie.create({ name });
    return res.json(movie);
  } catch (err) {
    return res.json(err);
  }
});

app.post("/movie/:id/actors", async (req, res) => {
  const id = req.params.id;
  const { name } = req.body;
  try {
    const movie = await Movie.findOne({
      where: {
        id,
      },
    });
    const actor = await Actor.create({ name });
    const movieActor = await MovieActor.create({
      movieId: movie.id,
      actorId: actor.id,
    });
    return res.json(actor);
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
});

app.get('/movie/:id/actors', async(req, res) => {
  const id = req.params.id;
  try {
    const movie = await MovieActor.findAll({
      where: {
        movieId: id
      },
      include: [Movie, Actor]
    })
    // const actor = await Actor.create({name});
    // const movieActor = await MovieActor.create({movieId: movie.id, actorId: actor.id})
    return res.json(movie);
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
})

app.get('/actor/:id/movies', async(req, res) => {
  const id = req.params.id;
  try {
    const actor = await MovieActor.findAll({
      where: {
        actorId: id
      },
      include: [Movie, Actor]
    })
    // const actor = await Actor.create({name});
    // const movieActor = await MovieActor.create({movieId: movie.id, actorId: actor.id})
    return res.json(actor);
  } catch (error) {
    console.log(error);
    return res.json(error);
  }
})

app.post("/actor", async (req, res) => {
  const { name } = req.body;
  try {
    const actor = await Actor.create({ name });
    return res.json(actor);
  } catch (err) {
    return res.json(err);
  }
});

app.post("/movieactor", async (req, res) => {
  const { movieId, actorId } = req.body;
  try {
    const movieActor = await MovieActor.create({ movieId, actorId });
    return res.json(movieActor);
  } catch (err) {
    return res.json(err);
  }
});

//   app.get("/movieactor", async (req, res) => {
//     try {
//       const movieActor = await MovieActor.findAll({
//         attributes: ['id'],
//         include: [{
//           model: Movie,
//           attributes: ['name'],
//         }, {
//           model: Actor,
//           attributes: ['name'],
//         }],
//       });
//       return res.json(movieActor);
//     } catch (err) {
//       return res.json(err);
//     }
//   });

app.get("/movieactor", async (req, res) => {
  try {
    const movieActor = await MovieActor.findAll({
      include: [Movie, Actor],
    });
    return res.json(movieActor);
  } catch (err) {
    return res.json(err);
  }
});

app.listen({ port: 8080 }, async () => {
  console.log("Server hosted on localhost:5000");
  await sequelize.authenticate();
  console.log("Databse Connected");
});
